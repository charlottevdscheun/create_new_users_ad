Import-Module ActiveDirectory

$SecPath = "OU=Domain Controllers,DC=FABRIKAM,DC=COM" 

$Group = $args[0]
$Class = $args[1]
$Filename = $args[2]


$Appstream = 'AppStream 2.0 - Workspace client'

#Create security map
if (-not (Get-ADOrganizationalUnit -Filter "Name -eq '$($Group)'" -ErrorAction SilentlyContinue)) {
    New-ADOrganizationalUnit -Name $Group -Path $SecPath -ProtectedFromAccidentalDeletion $False
    "Map $Group added "
}
else {
    "Map $Group already exists"
}

#Create security group in map
$SecPath = "OU=" +$Group+"," + $SecPath
if (-not(Get-ADGroup -Filter "Name -eq '$($Class)'" -ErrorAction SilentlyContinue)){
        New-ADGroup -Name $Class -Path $SecPath -GroupScope Global
        "Group $Class added"
        
  }
else{
    "Security group '$($Class)' already exists"
 }
#add group to sec-path
$SecPath = "CN="+$Class+"," +$SecPath


#Create class with users
$Path = "U=UserAccounts,DC=FABRIKAM,DC=COM" 

if (-not (Get-ADOrganizationalUnit -Filter "Name -eq '$($Class)'" -ErrorAction SilentlyContinue)) {
    New-ADOrganizationalUnit -Name $Class -Path $Path -ProtectedFromAccidentalDeletion $False
}
else {
    "Group '$($Class)' already exists"
}

$Path ="OU=" +$Class+"," + $Path

$ADUsers = Import-csv C:\Users\admin\Documents\Inloggegevens\$Filename.csv

#Loop through each row containing user details in the CSV file 
foreach ($User in $ADUsers)
{
	#Read user data from each field in each row and assign the data to a variable as below
		
	$Username 	= $User.Username
	$Password 	= $User.Password
	$Firstname 	= $User.Firstname
	$Lastname 	= $User.Lastname
    $Email      = $User.Email
	$OU 		= $Path #This field refers to the OU the user account is to be created in
    

	#Check to see if the user already exists in AD
	if (-not(Get-ADUser -F {SamAccountName -eq $Username}))
	{
        	#User does not exist then proceed to create the new user account
		"$Username will be created!"

        	#Account will be created in the OU provided by the $OU variable read from the CSV file
	     	New-ADUser `
            	-SamAccountName $Username `
            	-UserPrincipalName "$Username@email.com" `
            	-Name "$Firstname $Lastname" `
            	-GivenName $Firstname `
            	-EmailAddress $Email `
            	-Surname $Lastname `
            	-Enabled $True `
            	-DisplayName "$Lastname, $Firstname" `
            	-Path $Path `
            	-AccountPassword (convertto-securestring $Password -AsPlainText -Force) -ChangePasswordAtLogon $False -PasswordNeverExpires $True
	
		#Add to class sec group
        Add-ADGroupMember -Identity $SecPath -Members $Username
		#Add to appstream sec group	
		Add-ADGroupMember -Identity $Appstream -Members $Username
	
        	"$Username added to $Class security group"
	}
	else
	{
		 #If user does exist, give a warning
		"A user account with username $Username already exist in Active Directory."          
            
	}


}





import pandas as pd
from password_generator import PasswordGenerator
import sys
import os
import subprocess

#password creating with configurations
pwo = PasswordGenerator()
pwo.minlen      = 20
pwo.minnumbers  = 2
pwo.minschars   = 2
pwo.maxlen      = 20
pwo.minuchars   = 2
pwo.excludeschars = ",;|.'""" 

#Ask questions
file_name   = input("Type in the file name you stored the users (C:\\Users\\admin\\Documents\\Users\\xxxxx.csv): ")
group_name  = input("What is the group name: ")
badge_name  = input("What badge is this: ")
class_name  = input("What is the class name: ")
date_begin  = input("Which date does the class begin: ")
date_end    = input("Which date does it end, and can the workspace close: ")

Class = group_name + " - "+ badge_name+ " - "+ class_name


def read_csv(file):
    
    users = pd.read_csv(file, header = None, names=['Firstname', 'Lastname'], index_col=None, encoding="latin1")
    if users.isnull().values.any():
        print("")
        print("!!ERROR!!")
        print("can't read file, are you sure the names are seperated by a comma?")
        print("like: firstname, lastname")
        input("Press Enter to restart...")
        restart_program()
    else:
        create_user(users)
        
def create_user(users):#create username users
    users['Username'] = users["Firstname"].str[:1]  + users["Lastname"].str.replace(" ","")
    users['Username'] = users['Username'].str.lower()

    #create random password
    users['Password'] =users.apply(lambda x:  pwo.generate(), axis=1) 

    #save
    output = r"C:\\Users\admin\\Documents\\Inloggegevens\\"+file_name+".csv"
    users.to_csv(output,  index=False, encoding = 'latin1')
    print("Done")

    arguments =  group_name + '|"'+ Class +'"'
    # WATCH OUT FOR:
    # spaces in front en end
    # accenten op letters
    file_name2 = '"'+file_name+'" '
    group_name2 = '"'+group_name+'" '
    Class2 = '"'+Class+'" '
    arguments =  group_name2 + Class2 + file_name2
    print("SUCCESS, saved usernames and passwords at the following location:"+output)
    print("Start to insert into Directory ")
    p = subprocess.Popen([r"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe", r"C:\Users\admin\Documents\Scripts\AD-Users.ps1", arguments])
    p.wait()
    input("Finished, enter to close.....")
    
def restart_program():
    """Restarts the current program.
    Note: this function does not return. Any cleanup action (like
    saving data) must be done before calling this function."""
    python = sys.executable
    os.execl(python, python, * sys.argv)



#read the file
f = r'C:\\Users\\admin\\Documents\Users\\' + file_name +".csv"
if not os.path.exists(f):
    print("!!ERROR!!")
    print("can't find the file " +file_name+ " entered")
    input("Press Enter to restart...")
    restart_program()
else:
    read_csv(f)
